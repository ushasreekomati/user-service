package com.epam.userservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import com.epam.userservice.dto.UserDTO;
import com.epam.userservice.entity.User;
import com.epam.userservice.exception.UserNotFoundException;
import com.epam.userservice.repository.UserRepository;

@ExtendWith(SpringExtension.class)
class UserServiceTest {

	private UserService userService;

	@Mock
	private UserRepository userRepository;

	@BeforeEach
	public void before() {
		userService = new UserService(userRepository);
	}

	@Test
	void givenUsers_whenFindAll_thenReturnUsers() {
		final User userInDB = new User(1L, "Name1", "EmailId");
		given(this.userRepository.findAll()).willReturn(Collections.singletonList(userInDB));

		List<UserDTO> users = userService.getUsers();

		assertThat(users.size()).isEqualTo(1);
		assertThat(users.get(0).getUserId()).isEqualTo(1L);
		assertThat(users.get(0).getName()).isEqualTo("Name1");
		assertThat(users.get(0).getEmailId()).isEqualTo("EmailId");
		assertThat(users.get(0).toString()).isEqualTo("UserDTO [userId=" + userInDB.getUserId() + ", name="
				+ userInDB.getName() + ", emailId=" + userInDB.getEmailId() + "]");
	}

	@Test
	void givenUser_whenUpdate_thenReturnUser() {
		final User userInDB = new User(1L, "Name1", "EmailId");
		assertThat(userInDB.toString()).isEqualTo("User [userId=" + userInDB.getUserId() + ", name="
				+ userInDB.getName() + ", emailId=" + userInDB.getEmailId() + "]");

		given(this.userRepository.findById(1L)).willReturn(Optional.of(userInDB));
		final User updatedUserInDB = new User(1L, "Updated Name1", "Updated EmailId");
		given(this.userRepository.save(any(User.class))).willReturn(updatedUserInDB);

		final UserDTO userToUpdate = new UserDTO(1L, "Updated Name1", "Updated EmailId");
		final UserDTO updatedUser = userService.update(1L, userToUpdate);

		assertThat(updatedUser.getUserId()).isEqualTo(1L);
		assertThat(updatedUser.getName()).isEqualTo("Updated Name1");
		assertThat(updatedUser.getEmailId()).isEqualTo("Updated EmailId");
	}

	@Test
	void whenCreateUser_thenReturnUser() {
		given(this.userRepository.save(any(User.class))).willReturn(new User(1L, "Name1", "EmailId"));

		final UserDTO user = new UserDTO(1L, "Name1", "EmailId");
		UserDTO createdUser = userService.addNew(user);

		assertThat(createdUser.getUserId()).isEqualTo(1L);
		assertThat(createdUser.getName()).isEqualTo("Name1");
		assertThat(createdUser.getEmailId()).isEqualTo("EmailId");
	}

	@Test
	void givenUser_whenFindById_thenReturnuUser() {
		final User userInDB = new User();
		userInDB.setUserId(1L);
		userInDB.setName("Name1");
		userInDB.setEmailId("EmailId");
		given(this.userRepository.findById(1L)).willReturn(Optional.of(userInDB));

		UserDTO user = userService.getUserById(1L);

		assertThat(user.getUserId()).isEqualTo(1L);
		assertThat(user.getName()).isEqualTo("Name1");
		assertThat(user.getEmailId()).isEqualTo("EmailId");
		assertThat(user.toString()).isEqualTo("UserDTO [userId=" + user.getUserId() + ", name=" + user.getName()
				+ ", emailId=" + user.getEmailId() + "]");

	}

	@Test
	void givenNoUser_whenFindById_thenReturnError() {
		given(this.userRepository.findById(1L)).willReturn(Optional.empty());

		assertThatCode(() -> userService.getUserById(1L)).isInstanceOf(UserNotFoundException.class)
				.hasMessageContaining("User not found: 1");
	}

	@Test
	void givenNoUser_whenUpdate_thenReturnError() {
		given(this.userRepository.findById(1L)).willReturn(Optional.empty());

		assertThatCode(() -> userService.update(1L, new UserDTO())).isInstanceOf(UserNotFoundException.class)
				.hasMessageContaining("User not found: 1");
	}

	@Test
	void givenUser_whenDelete_thenReturnVoid() {
		final User userInDB = new User(1L, "Name1", "EmailId");
		given(this.userRepository.findById(1L)).willReturn(Optional.of(userInDB));
		willDoNothing().given(this.userRepository).delete(any(User.class));

		userService.delete(1L);
	}

	@Test
	void givenNoUser_whenDelete_thenReturnError() {
		given(this.userRepository.findById(1L)).willReturn(Optional.empty());

		assertThatCode(() -> userService.delete(1L)).isInstanceOf(UserNotFoundException.class)
				.hasMessageContaining("User not found: 1");
	}
}
