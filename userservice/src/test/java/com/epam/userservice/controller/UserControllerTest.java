package com.epam.userservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import com.epam.userservice.dto.UserDTO;
import com.epam.userservice.exception.UserNotFoundException;
import com.epam.userservice.service.UserService;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
class UserControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UserService userService;

	@Test
	void givenUsers_whenGetUsers_thenStatus200() throws Exception {
		List<UserDTO> users = new ArrayList<>();
		users.add(new UserDTO(1L, "Name1", "EmailId"));
		users.add(new UserDTO(2L, "Name2", "EmailId2"));
		given(this.userService.getUsers()).willReturn(users);
		this.mvc.perform(get("/users").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	void givenUser_whenGetUserById_thenStatus200() throws Exception {
		UserDTO user = new UserDTO(1L, "Name", "EmailId");

		given(this.userService.getUserById(1L)).willReturn(user);

		this.mvc.perform(get("/users/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.name", is("Name")));
	}

	@Test
	void givenUserNotExists_whenGetByUserId_thenStatus404() throws Exception {
		given(this.userService.getUserById(1L)).willThrow(new UserNotFoundException("User not found"));

		this.mvc.perform(get("/books/1")).andExpect(status().isNotFound());
	}

	@Test
	void givenUser_whenUpdateUser_thenStatus200() throws Exception {
		UserDTO user = new UserDTO(1L, "Updated Name", "Updated EmailId");
		given(this.userService.update(anyLong(), any(UserDTO.class))).willReturn(user);

		String userJson = "{\"userId\":1, \"name\": \"Updated Name\", \"emailId\": \"Updated EmailId\"}";
		final MockHttpServletRequestBuilder request = put("/books/1").content(userJson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		this.mvc.perform(request).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is("Updated Name")))
				.andExpect(jsonPath("$.emailId", is("Updated EmailId")));

	}

	@Test
	void whenAddNewUser_thenStatus201() throws Exception {
		UserDTO user = new UserDTO(1L, "Name", "EmailId");
		given(this.userService.addNew(any(UserDTO.class))).willReturn(user);

		String userJson = "{\"name\": \"Name\", \"emailId\": \"EmailId\"}";
		final MockHttpServletRequestBuilder request = post("/users").content(userJson)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		this.mvc.perform(request).andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(header().exists("Location"))
				.andExpect(jsonPath("$.name", is("Name")));
	}

	@Test
	void givenUser_whenDeleteUser_thenStatus204() throws Exception {
		willDoNothing().given(this.userService).delete(1L);
		this.mvc.perform(delete("/users/1")).andExpect(status().isNoContent());
	}
}