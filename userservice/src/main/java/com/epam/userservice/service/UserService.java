package com.epam.userservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.userservice.dto.UserDTO;
import com.epam.userservice.entity.User;
import com.epam.userservice.exception.BadRequestException;
import com.epam.userservice.exception.UserNotFoundException;
import com.epam.userservice.repository.UserRepository;
import java.util.function.Supplier;
import static java.util.stream.Collectors.toList;

@Service
public class UserService {

	UserRepository userRepository;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<UserDTO> getUsers() {
		return userRepository.findAll().stream().map(UserDTO::new).collect(toList());
	}

	public UserDTO addNew(UserDTO userDTO) {
		User user = new User(userDTO.getName(), userDTO.getEmailId());
		return new UserDTO(userRepository.save(user));
	}

	public UserDTO update(Long userId, UserDTO userDTO) {
		if (!userDTO.getUserId().equals(userId)) {
			throw new BadRequestException("User Id's Did not match");
		}
		User user = new User(userId, userDTO.getName(), userDTO.getEmailId());
		return userRepository.findById(userId).map(existingUser -> userRepository.save(user)).map(UserDTO::new)
				.orElseThrow(userNotFound(userId));
	}

	public UserDTO getUserById(Long userId) {
		return userRepository.findById(userId).map(UserDTO::new).orElseThrow(userNotFound(userId));
	}

	public void delete(Long userId) {
		userRepository.deleteById(userId);
	}

	private Supplier<UserNotFoundException> userNotFound(Long userId) {
		return () -> new UserNotFoundException("User not found: " + userId);
	}
}
