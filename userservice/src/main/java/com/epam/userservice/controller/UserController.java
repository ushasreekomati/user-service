package com.epam.userservice.controller;

import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.epam.userservice.dto.UserDTO;
import com.epam.userservice.entity.User;
import com.epam.userservice.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/users")
@RestController

@Api(value = "User Resource REST Endpoint", description = "Shows the user info")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping
	@ApiOperation(value = "Get Users", notes = "Fetches all the Users", response = User.class, responseContainer = "List")
	@ApiResponse(code = 200, message = "Users Fetched Sucessfully")
	public ResponseEntity<List<UserDTO>> getUsers() {
		return ResponseEntity.ok(userService.getUsers());
	}

	@PostMapping
	@ApiOperation(value = "Add New User", notes = "Adds new User into Users", response = User.class, responseContainer = "List")
	@ApiResponse(code = 201, message = "New User Created")
	public ResponseEntity<UserDTO> addNew(@RequestBody UserDTO userDTO) {

		UserDTO savedUser = userService.addNew(userDTO);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/id")
				.buildAndExpand(savedUser.getUserId()).toUri();

		return ResponseEntity.created(location).body(savedUser);
	}

	@PutMapping("{userId}")
	@ApiOperation(value = "Update Existing User", notes = "Updates the details of Existing User", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "User not found"),
			@ApiResponse(code = 200, message = "User Updated"),
			@ApiResponse(code = 400, message = "Invalid User is supplied") })
	public ResponseEntity<UserDTO> update(@PathVariable Long userId, @RequestBody @Valid UserDTO userDTO) {

		return ResponseEntity.ok(userService.update(userId, userDTO));

	}

	@GetMapping("{userId}")
	@ApiOperation(value = "Get User By User Id", notes = "Gets User By User Id from the List of Users", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 404, message = "User not found"),
			@ApiResponse(code = 200, message = "Successfully Retrived") })
	public ResponseEntity<UserDTO> get(@PathVariable Long userId) {
		return ResponseEntity.ok(userService.getUserById(userId));
	}

	@DeleteMapping("{userId}")
	@GetMapping("{userId}")
	@ApiOperation(value = "Delete User", notes = "Deletes User By User Id from the List of Users")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "User not found"),
			@ApiResponse(code = 204, message = "User deleted successfully") })
	public ResponseEntity<?> delete(@PathVariable Long userId) {
		userService.delete(userId);
		return ResponseEntity.noContent().build();
	}
}
