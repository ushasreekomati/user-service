package com.epam.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.userservice.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
