package com.epam.userservice.dto;

import javax.validation.constraints.Size;

import com.epam.userservice.entity.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "User DTO")

public class UserDTO {

	@ApiModelProperty(notes = "UserId of the User")
	Long userId;
	@ApiModelProperty(notes = "Name of the User")
	@Size(min = 3)
	String name;
	@ApiModelProperty(notes = "email Id of the User")
	@Size(min = 3)
	String emailId;

	public UserDTO() {
	}

	public UserDTO(User user) {
		this.userId = user.getUserId();
		this.name = user.getName();
		this.emailId = user.getEmailId();
	}

	public UserDTO(Long userId, String name, String emailId) {
		this.userId = userId;
		this.name = name;
		this.emailId = emailId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public String toString() {
		return "UserDTO [userId=" + userId + ", name=" + name + ", emailId=" + emailId + "]";
	}

}
