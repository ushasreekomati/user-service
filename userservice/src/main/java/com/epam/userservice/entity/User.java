package com.epam.userservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long userId;
	@Column(length = 50, nullable = false)
	String name;
	@Column(length = 50, nullable = false)
	String emailId;

	public User() {
	}

	public User(Long userId, String name, String emailId) {
		this.userId = userId;
		this.name = name;
		this.emailId = emailId;
	}

	public User(String name, String emailId) {
		this.name = name;
		this.emailId = emailId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", emailId=" + emailId + "]";
	}

}
